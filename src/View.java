public class View {
    public static final String GREETING = "Hello!";
    public static final String ERROR = "Oh no :( There is something wrong in ";
    public static final String TRY_AGAIN = "Please, try again)";
    public static final String FINISH = "Yes! Thank you, we wrote your data in our system:";
    public static final String INPUT_NAME = "Enter please your name (2 words, only letters)";
    public static final String INPUT_NICKNAME = "Okay, now enter your nickname " +
            "(just letters and '_' from 6 to 20 symbols)";

    public void printMessage(String message) {
        System.out.println(message);
    }

    public void printMessage(String message, String... data) {
        System.out.println(message);
        for(String d: data){
            System.out.println(d);
        }
    }

    public void printError(String message) {
        System.out.print(ERROR);
        System.out.println(message);
    }

    public void printError(String... messages) {
        System.out.print(ERROR);
        for (int i = 0; i < messages.length; i++) {
            if (i == messages.length - 1) {
                System.out.println("and " + messages[i]);
                break;
            }
            if (messages[i] != null) System.out.print(messages[i] + ", ");
        }
    }
}
