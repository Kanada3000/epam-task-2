import java.util.regex.Pattern;

public class Model {
    private String name;
    private String nickname;

    public Model() {

    }

    public Model(String name, String nickname) {
        this.name = name;
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public boolean checkName(String name) {
        return Pattern.compile("^[a-zA-Z]{2,} [a-zA-Z]{2,}$").matcher(name).find();
    }

    public boolean checkNickname(String nickname) {
        return Pattern.compile("^\\w{6,20}$").matcher(nickname).find();
    }
}
