import java.util.Arrays;
import java.util.Scanner;

public class Controller {

    private Model model;
    private View view;

    public Controller() {
    }

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void run() {
        Scanner sc = new Scanner(System.in);

        view.printMessage(View.GREETING);

        model = writeCorrectData(sc);

        view.printMessage(View.FINISH, model.getName(), model.getNickname());
    }

    /**
     * Read String from console using java.util.Scanner.
     *
     * @param sc the Scanner
     * @return the input String
     * @see java.util.Scanner
     */
    private String getInputValue(Scanner sc) {
        if (sc.hasNextLine()) return sc.nextLine();
        return null;
    }

    /**
     * Read "name" and "nickname" from console, validate it and create Model if all right.
     * Else, repeat.
     *
     * @param sc the Scanner
     * @return the Model with correct data
     * @see java.util.Scanner
     */
    private Model writeCorrectData(Scanner sc) {
        String name = null;
        String nickname = null;

        while (name == null && nickname == null) {
            view.printMessage(View.INPUT_NAME);
            name = getInputValue(sc);
            if (name != null && !name.isEmpty() && !name.isBlank()) {
                if (!model.checkName(name)) name = null;
            } else name = null;

            view.printMessage(View.INPUT_NICKNAME);
            nickname = getInputValue(sc);
            if (nickname != null && !nickname.isEmpty() && !nickname.isBlank()) {
                if (!model.checkNickname(nickname)) nickname = null;
            } else nickname = null;

            if (name == null || nickname == null) {
                view.printError(name == null ? "name" : null, nickname == null ? "nickname" : null);
                view.printMessage(View.TRY_AGAIN);
            }
        }
        return new Model(name, nickname);
    }
}
